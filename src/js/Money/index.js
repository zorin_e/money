import Calculator from 'js/Money/services/calculator'
import Format from 'js/Money/services/format'
import Converter from 'js/Money/services/converter'
import { Defaults } from 'js/Money/services/settings'

const calculator = Calculator()

/**
 * @param {number} [options.amount=0] - amount in minor units default 0
 * @param {string} [options.currency='RUB'] - currency default RUB
 * @param {string} [options.currency='ru-RU'] - locale default ru-RU
 * @param {number} [options.currency=2] - precision default 2
 * @return {Object}
 */
const Money = (options = {}) => {
    // get defaults options
    const { amount, currency, precision } = Object.assign(
        {
            amount: Money.defaultAmount,
            currency: Money.defaultCurrency,
            precision: Money.defaultPrecision
        },
        options
    )

    const { defaultLocale } = Money

    /**
     * for chaining
     * @param {number} [options.amount] - new amount
     * @param {string} [options.amount] - new currency
     * @param {string} [options.locale] - new locale
     * @return {object} money
     */
    const cr = function(options) {
        const obj = Object.assign(
            Object.assign({ amount, currency, precision }, options),
            Object.assign({ locale: this.locale }, options)
        )

        return Object.assign(
            Money({
                amount: obj.amount,
                currency: obj.currency,
                precision: obj.precision
            }),
            {
                locale: obj.locale
            }
        )
    }

    return {
        /**
         * @return {number}
         */
        getAmount() {
            return amount
        },
        /**
         * @return {string}
         */
        getCurrency() {
            return currency
        },
        /**
         * @return {object} amount after multiply
         */
        multiply(value) {
            return cr.call(this, {
                amount: calculator.multiply(
                    this.getAmount(),
                    value
                )
            })
        },
        /**
         * @return { object } amount after subtract
        */
        subtract(value) {
            return cr.call(this, {
                amount: calculator.subtract(
                    this.getAmount(),
                    value
                )
            })
        },
        /**
         * @return { object } amount after add
        */
       add(value) {
           return cr.call(this, {
               amount: calculator.add(
                   this.getAmount(),
                   value
               )
           })
       },
        /**
         * @return { object } amount after divide
         */
        divide(value) {
            return cr.call(this, {
                amount: calculator.divide(
                    this.getAmount(),
                    value
                )
            })
        },
        /**
         * @param {string} value new locale value
         * @return {object} locale after set
         */
        setLocale(value) {
            return cr.call(this, { locale: value })
        },
        /**
         * @return {string} locale
         */
        getLocale() {
            return this.locale || defaultLocale
        },
        getPrecision() {
            return precision
        },
        toUnit() {
           return calculator
            .divide(this.getAmount(), 100)
            .toFixed(this.getPrecision())
        },
        /**
         * returns formatted amount, depends on currency, locale
         */
        toFormat() {
            return Format({
                units: this.toUnit(),
                currency: this.getCurrency(),
                precision: this.getPrecision(),
                locale: this.getLocale()
            })
        },
        /**
         *
         * @param {stirng} currency to convert
         * @return {object} money
         */
        convert(currency) {
            return Converter()
                .getRate(this.getCurrency(), currency)
                .then(rate => {
                    try {
                        rate = rate[`${this.getCurrency()}_${currency}`].val
                        return cr.call(this, {
                            amount: Math.round(calculator.multiply(this.getAmount(), parseFloat(rate))),
                            currency
                        })
                    }
                    catch(err) {
                        throw new TypeError(`${currency} rate not found`)
                    }

                })
        },
        equalsTo(comparator) {
            return this.hasSameAmount(comparator) && this.hasSameCurrency(comparator)
        },
        hasSameAmount(comparator) {
            return this.getAmount() === comparator.getAmount()
        },
        hasSameCurrency(comparator) {
            return this.getCurrency() === comparator.getCurrency()
        }
    }
}

export default Object.assign(Money, Defaults)