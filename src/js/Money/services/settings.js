export const Defaults = {
    defaultAmount: 0,
    defaultCurrency: 'RUB',
    defaultLocale: 'ru-RU',
    defaultPrecision: 2
}