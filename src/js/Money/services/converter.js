import { sendReq } from 'js/Money/services/helpers'

export default function Converter() {
    return {
        async getRate(from, to) {
            const data = await sendReq({
                url: `http://free.currencyconverterapi.com/api/v5/convert?q=${from}_${to}&compact=y`
            })

            return data
        }
    }
}