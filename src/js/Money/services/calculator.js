export default function Calculator() {
    return {
        /**
         * returns the product of two numbers
         * @param {number} a 
         * @param {number} b 
         * @return {number}
         */
        multiply(a, b) {
            return a * b
        },

        /**
         * returns the sum
         * @param {number} a 
         * @param {number} b
         * @return {number}
         */
        add(a, b) {
            return a + b
        },

        /**
         * returns the difference
         * @param {number} a
         * @param {number} b
         * @return {number}
         */
        subtract(a, b) {
            return a - b
        },

        /**
         * returns the quotient
         * @param {number} a
         * @param {number} b
         * @return {number}
         */
        divide(a, b) {
            return a / b
        }
    }
}