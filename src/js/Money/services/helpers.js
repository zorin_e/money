// import 'whatwg-fetch'

/**
 * send request to server using fetch
 * @param {string} [options.url] url
 * @param {string} [options.method] request method
 * @param {string} [options.credentials] send headers, cookies with request or no 
 * @param {string|json} [options.body] body if options.method !== 'GET'
 * @param {object} [options.headers]
 * @return {object} data
 */
const sendReq = async ({
    url,
    ...options
}) => {
    const req = await fetch(url, options)
    const data = await req.json()
    return data
}

export {
    sendReq
}