import Intl from 'intl'
import 'intl/locale-data/jsonp/ru.js'

export default function Format({ units, currency, locale, precision }) {
    const formatter = new Intl.NumberFormat(locale, {
        style: 'currency',
        currency: currency,
        minimumFractionDigits: precision
    })
    return formatter.format(units)
}