import 'styles/index.scss'
import "babel-polyfill";

import Money from 'js/Money'

// examples

// math, format, get
const money = Money({ amount: 50, precision: 2, currency: 'RUB' })
    .add(50)
    .multiply(10)
    .divide(2)
    .subtract(50)

    console.log(money.getAmount()) // 450
    console.log(money.toFormat()) // 4,50 P

// get rate and convert
;(async () => {
    const moneyConverted = await Money({ amount: 58000 }).convert('EUR')
    console.log(moneyConverted.getCurrency()) // EUR
    console.log(moneyConverted.getAmount()) // 766
    console.log(moneyConverted.toUnit()) // 7.66
})()

// same amount
const sameAmount = Money({ amount: 50, precision: 2, currency: 'RUB' }).hasSameAmount(Money({ amount: 500, precision: 2, currency: 'RUB' }))
console.log(sameAmount) //false

// same currency
const sameCurrency = Money({ amount: 50, precision: 2, currency: 'RUB' }).hasSameCurrency(Money({ amount: 500, precision: 2, currency: 'RUB' }))
console.log(sameCurrency) // true

// equals
const equalsTo = Money({ amount: 50, precision: 2, currency: 'RUB' }).equalsTo(Money({ amount: 50, precision: 2, currency: 'RUB' }))
console.log(equalsTo) //true