const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const NODE_ENV = process.env.NODE_ENV
const PRODUCTION = NODE_ENV === 'production'

const plugins = [
	new webpack.DefinePlugin({
		'process.env': {
			NODE_ENV: JSON.stringify(NODE_ENV)
		}
	}),
	new HtmlWebpackPlugin({
		template: './index.html',
		filename: 'index.html'
	}),
	new ScriptExtHtmlWebpackPlugin({
    	defaultAttribute: 'defer'
  	}),
	new ExtractTextPlugin('bundle.[chunkhash:10].css')
]

const CONF = {
	entry: ['./src/js/index.js'],
	output: {
		path: path.resolve(__dirname, './dist'),
		filename: 'bundle.[chunkhash:10].js',
		publicPath: '/'
	},
	resolve: {
		modules: [path.resolve(__dirname, './src'), 'node_modules']
	},
	plugins,
	module: {
		rules: [
			{
				test: /\.html$/,
				use: [
					{
						loader: "html-loader",
						options: { minimize: PRODUCTION }
					}
				]
			 },
			{
				test: /\.js$/,
				loader: 'babel-loader',
				include: [path.resolve(__dirname, './src')]
			},
			{
				test: /\.scss/,
				use: ExtractTextPlugin.extract({
					use: [
						{
							loader: 'css-loader',
							options: {
								sourceMap: !PRODUCTION
							}
						},
						{
							loader: 'sass-loader',
							options: {
								sourceMap: !PRODUCTION
							}
						},
						'postcss-loader'
					]
				})
				
			},
			{ test: /\.gif$/, loader: 'url-loader?limit=10000&mimetype=image/gif' },
			{ test: /\.jpg$/, loader: 'url-loader?limit=10000&mimetype=image/jpg' },
			{ test: /\.png$/, loader: 'url-loader?limit=10000&mimetype=image/png' },
			{ test: /\.svg/, loader: 'url-loader?limit=26000&mimetype=image/svg+xml' },
			{ test: /\.(woff|woff2|ttf|eot)/, loader: 'url-loader?limit=1' }
		]
	}
}

module.exports = CONF