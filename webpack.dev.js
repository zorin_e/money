const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const path = require('path')

module.exports = merge(common, {
	devtool: 'source-map',
	devServer: {
		overlay: true,
		contentBase: path.resolve(__dirname, 'dist'),
		publicPath: '/'
	},
	mode: 'development'
})