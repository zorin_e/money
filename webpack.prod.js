const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const ImageminPlugin = require('imagemin-webpack-plugin').default

const plugins = [
	new CleanWebpackPlugin('dist', {
		root: __dirname,
		verbose: true
	}),
  	new ImageminPlugin({
      minFileSize: 10000, // Only apply this one to files over 10kb
      jpegtran: { progressive: true }
    })
]

module.exports = merge(common, {
	plugins,
	optimization: {
		minimizer: [
			new UglifyJsPlugin({
				uglifyOptions: {
					cache: true,
					parallel: true,
					output: { comments: false }
				}
			}),
			new OptimizeCSSAssetsPlugin()
		],
		splitChunks: {
			cacheGroups: {
				default: false,
				commons: {
				test: /[\\/]node_modules[\\/]/,
				name: 'vendors',
				chunks: 'all',
				minChunks: 2
				}
			}
		}
	},
	mode: 'production'
})